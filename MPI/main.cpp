//
//  main.cpp
//  PT_MPI
//
//  Created by liyuezhen on 2020/7/21.
//  Copyright © 2020 liyuezhen. All rights reserved.
//

#include <iostream>
#include <math.h>
#include <random>
#include <fstream>
#include <string>
#include <vector>
#include "mpi.h"

using std::cout;
using std::cin;
using std::endl;
using namespace std;

int main(int argc, char * argv[]) {
    int x, y = 28, master = 0; // y: number of replicas, x: rank
    int step;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&x);
    MPI_Comm_size(MPI_COMM_WORLD,&y);
    
    
    
    
    ofstream myfile;
    myfile.open("PT.txt");
    if (!myfile){
        //cout << "sth wrong" << endl;
    }
    double prob;
    int M_max = 100000;
    
    std::random_device rd;
    std::mt19937 generate = std::mt19937(271828); // remember to switch back to rd()
    std::uniform_real_distribution<float> conti(0,1);
    
    // Defining which link belongs to each plaquettes. In the content below, ijkl are ordered by their values from small to large.
    
    
    // Variables declaration
    double V = 0.5;
    int num_FP = 0, num_updating_line = 0, num_FP_after = 0;
    int N = 16, base = 50; // N: system size, n: number of operators
    int nn[y], M, n;
    int Nb = 2 * N * N;
    int betabeta[y];//, betabeta_temp[y];
    //int i[N * N], j[N * N], k[N * N], l[N * N]; // the bond surrounding each plaquette.
    //int s[M_max], s_new[M_max];
    //int *ss[y], *s_temp[y], *linklink[y], *link_temp[y];
    int u, v, p, b, q;
    // constants used as tags
    
    int tg_energy = 1;
    int tg_n = 2;
    int tg_psix = 3;
    int tg_psiy = 4;
    
    
    vector<int> row_s;
    vector<int> row_link;
    row_s.assign(M_max, -1);
    row_link.assign(Nb, 0);
    //vector<vector<int>> ss;
    //vector<vector<int>> s_temp;
    //vector<vector<int>> linklink;
    //vector<vector<int>> link_temp;
    //ss.assign(y, row_s);
    //s_temp.assign(y, row_s);
    //linklink.assign(y, row_link);
    //link_temp.assign(y, row_link);
    //vector<int> nn;
    //vector<int> betabeta;
    vector<int> i;
    vector<int> j;
    vector<int> k;
    vector<int> l;
    vector<int> s;
    vector<int> s_new;
    vector<double> EE;
    vector<double> psixpsix;
    vector<double> psiypsiy;
    
    //nn.assign(y, 0);
    //betabeta.assign(y, 0);
    EE.assign(y, 0);
    psixpsix.assign(y, 0);
    psiypsiy.assign(y, 0);
    s.assign(M_max, -1);
    s_new.assign(M_max, -1);
    i.assign(Nb / 2, 0);
    j.assign(Nb / 2, 0);
    k.assign(Nb / 2, 0);
    l.assign(Nb / 2, 0);
    
    /*for (int i = 0; i < y; i ++){
     s_temp[i] = (int*)malloc(M_max*sizeof(int*));
     ss[i] = (int*)malloc(M_max*sizeof(int*));
     linklink[i] = (int*)malloc(Nb*sizeof(int*));
     link_temp[i] = (int*)malloc(Nb*sizeof(int*));
     }*/
    
    
    for (u = 0; u < y; u ++){
        betabeta[u] = (base + u);
        nn[u] = 0;
    }
    
    //int link[Nb], link_new[Nb], link0[Nb], link0new[Nb], updating_line[Nb], phase[Nb]; // updating_line[Nb]: a list recording if there's an updating line at a given link (1 = yes, 0 = no)
    vector<int> link;
    vector<int> link_new;
    vector<int> link0;
    vector<int> link0new;
    vector<int> updating_line;
    vector<int> phase;
    
    link.assign(Nb, 0);
    link_new.assign(Nb, 0);
    link0.assign(Nb, 0);
    link0new.assign(Nb, 0);
    updating_line.assign(Nb, 0);
    phase.assign(Nb, 0);
    
    int MC_sweep, MC_step = 800;
    
    int a = 0; // to determine if we have an acceptable off-diagonal update
    //int bin;
    int psi_x = 0, psi_y = 0;
    double avg_x = 0, avg_y = 0;
    double E; // energy
    // set up the initial configuration
    
    for (p = 0; p < Nb; p ++){
        if (p < Nb / 2 && p % 2 == 0){
            link[p] = 1;
        }
        else{
            link[p] = 0;
        }
    }
    
    
    //for (x = 0; x < y; x ++){ // x: the number of replica
    //number[x] = x;
    //MM[x] = 3000;
    //MM_temp[x] = 3000;
    /*for (int p = 0; p < M_max; p ++){
     ss[x][p] = -1;
     }*/
    if (x % 4 == 0){ // start from different configurations
        for (p = 0; p < Nb; p ++){
            if (p < Nb / 2 && p % 2 == 0){
                link[p] = 1;
            }
            else{
                link[p] = 0;
            }
        }
    }
    if (x % 4 == 1){
        for (p = 0; p < Nb; p ++){
            if (p < Nb / 2 && p % 2 == 1){
                link[p] = 1;
            }
            else{
                link[p] = 0;
            }
        }
    }
    if (x % 4 == 2){
        for (p = 0; p < Nb; p ++){
            if (p >= Nb / 2 && (p / N) % 2 == 0){
                link[p] = 1;
            }
            else{
                link[p] = 0;
            }
        }
    }
    if (x % 4 == 3){
        for (p = 0; p < Nb; p ++){
            if (p >= Nb / 2 && (p / N) % 2 == 1){
                link[p] = 1;
            }
            else{
                link[p] = 0;
            }
        }
    }
    //}
    
    for (p = 0; p < N * N; p ++){
        i[p] = p;
        j[p] = (p + N) % (N * N);
        k[p] = p + N * N;
        l[p] = k[p] / N * N + (k[p] % N + 1) % N;
        //cout << i[p] << " " << j[p] << " " << k[p] << " "  << l[p] << endl;
    }
    
    for (u = 0; u < Nb; u ++){
        if (u < Nb / 2){
            phase[u] = pow(-1, u);
        }
        else{
            phase[u] = pow(-1, u / N);
        }
    }
    
    
    
    // rotate!
    /*
     p = N * N;
     q = N * N + N - 1;
     for (x = 0; x < y; x ++){
     linklink[x][p] = 1;
     linklink[x][q] = 1;
     for (u = p - N * N; u <= q - N * N - 1; u ++){
     linklink[x][u] = 1 - linklink[x][u];
     }
     if (p / N == 2 * N - 1){ // to deal with the rightmost column...
     for (u = p - N * N - N * (N - 1); u <= q - N * N - N * (N - 1) - 1; u ++){
     linklink[x][u] = 1 - linklink[x][u];
     }
     }
     else{
     for (u = p - N * N + N; u <= q - N * N + N - 1; u ++){
     linklink[x][u] = 1 - linklink[x][u];
     }
     }
     }
     */
    
    
    
    
    // start the update
    //for (bin = 0; bin < 1; bin ++){
    //cout << "bin = " << bin << endl;
    
    M = 3000;
    n = 0;
    for (MC_sweep = 0; MC_sweep < MC_step; MC_sweep++){
        if (MC_sweep % 10 == 0 && x == 0){
            cout << "MC sweep = " << MC_sweep << endl;
        }
        //if (stepstep[0] % 10 == 0){ // conduct the swap step every 10 MC sweep
        if (x == 0){
            //cout << "swappp" << endl;
            for (u = 0; u < y - 1; u ++){
                for (v = 0; v < y; v ++){
                    if (betabeta[v] == base + u){
                        p = v;
                    }
                    if (betabeta[v] == base + u + 1){
                        b = v;
                    }
                }
                prob = conti(generate);
                if (prob < pow(1.0 * betabeta[b] / betabeta[p], nn[p] - nn[b])){
                    //cout << "swap " << n1 << " " << n2 << endl;
                    v = betabeta[b];
                    betabeta[b] = betabeta[p];
                    betabeta[p] = v;
                }
            }
            
            for (u = 0; u < y; u ++){
                cout << betabeta[u] << " ";
            }
            cout << endl;
            
            
            
            
            /*for (int i = 0; i < y; i ++){ // x = 0 is the core resreved for master and therefore should not be taken into account while swapping
             prob = conti(generate);
             //cout << nn[i] << " " << nn[i + 1] << endl;
             //cout << "prob = " << pow(1.0 * (base + i + 1) / (base + i), nn[i] - nn[i + 1]) << endl;
             if (prob < pow(1.0 * (base + i + 1) / (base + i), nn[i] - nn[i + 1])){
             int l = number[i];
             number[i] = number[i + 1];
             number[i + 1] = l;
             l = nn[i];
             nn[i] = nn[i + 1];
             nn[i + 1] = l;
             //cout << "swap " << i << " " << i + 1 << endl;
             }
             }*/
            
            // rearrange the order of operator string and configurations according to the order
            
            /*for (int i = 0; i < y; i ++){
             //MM_temp[i] = MM[number[i]];
             betabeta_temp[i] = betabeta[number[i]];
             for (int j = 0; j < Nb; j ++){
             link_temp[i][j] = linklink[number[i]][j];
             }
             for (int j = 0; j < M_max; j ++){
             s_temp[i][j] = ss[number[i]][j];
             }
             }*/
            
            
            /*for (int i = 0; i < y; i ++){
             //cout << "number ";
             //cout << number[i] << endl;
             MM[i] = MM_temp[i];
             //cout << "M " << MM[i] << endl;
             betabeta[i] = betabeta_temp[i];
             //cout << "beta ";
             //cout << betabeta[i] << endl;
             number[i] = i;
             }*/
        }
        //MPI_Bcast(&nn, y, MPI_INT, master, MPI_COMM_WORLD);
        //MPI_Bcast(&MM, y, MPI_INT, master, MPI_COMM_WORLD);
        MPI_Bcast(&betabeta, y, MPI_INT, master, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);
        //cout << "GO!" << endl;
        //}
        
        //n = nn[x];
        //cout << "n " << x << " = " << nn[x] << endl;
        //M = MM[x];
        //cout << "M " << x << " = " << MM[x] << endl;
        
        //cout << "parallel starts" << endl;
        for (step = 0; step < 9; step ++){ // parallel
            /*if (x == 0){
             cout << "x = 0" << endl;
             MPI_Barrier(MPI_COMM_WORLD);
             cout << "hello~";
             }*/
            //MPI_Barrier(MPI_COMM_WORLD);
            //if (x != 0){
            //for (int x = 0; x < y; x ++){ // x: number of replica
            //cout << "x = " << x << endl;
            
            
            
            double beta = betabeta[x] / 10.0;
            //cout << "step " << x << " = " << step << endl;
            // move all the variables to the public one
            //cout << "previous configuration" << endl;
            for (u = 0; u < Nb; u ++){
                //link[i] = linklink[x][i];
                link0[u] = link[u];
                link0new[u] = link[u];
                //cout << link[i] << " ";
            }
            //cout << endl;
            
            //cout << "previous operator string" << endl;
            /*for (int p = 0; p < M_max; p ++){
             s[p] = ss[x][p];
             //cout << s[p] << " ";
             }*/
            //cout << endl;
            
            // here we start a normal MC step
            /*q = 0;
             for (int i = 0; i < Nb; i ++){
             if (link[i] == 1){
             q ++;
             }
             }
             if (q != Nb / 4){
             for (u = 0; u < 100; u ++){
             cout << "error" << endl;
             }
             break;
             }*/
            //cout << "n = " << n << endl;
            
            // diagonal update
            
            /*cout << "diagonal update" << endl;
             //cout << "initial state" << endl;
             for (u = 0; u < Nb; u ++){
             if (link[u] == 1){
             //cout << u << " ";
             }
             }
             //cout << endl;*/
            for (p = 0; p < M; p ++){
                //cout << "p = " << p << endl;
                if (s[p] == -1){
                    //cout << "no operator" << endl;
                    b = floor(N * N * conti(generate)); // choose a plaquette
                    //cout << "b = " << b << endl;
                    if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                        //cout << "flippable" << endl;
                        if (conti(generate) < N * N * beta / (M - n)){
                            //cout << "insert an FP" << endl;
                            s[p] = 2 * b;
                            //cout << "s" << p << " = " << s[p] << endl;
                            n += 1;
                        }
                        //cout << "n = " << n << endl;
                    }
                    else{
                        if (conti(generate) < N * N * beta * (1 + V) / (M - n)){
                            s[p] = 2 * b;
                            //cout << "s" << p << " = " << s[p] << endl;
                            n += 1;
                            //cout << "n = " << n << endl;
                        }
                    }
                }
                else if (s[p] % 2 == 0){
                    b = s[p] / 2;
                    if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                        if (conti(generate) < (M - n + 1) / (N * N * beta)){
                            s[p] = -1;
                            //cout << "s" << p << " = " << s[p] << endl;
                            n -= 1;
                        }
                        //cout << "n = " << n << endl;
                    }
                    else{
                        if (conti(generate) < (M - n + 1) / (N * N * beta * (1 + V))){
                            s[p] = -1;
                            //cout << "s" << p << " = " << s[p] << endl;
                            n -= 1;
                            //cout << "n = " << n << endl;
                        }
                    }
                }
                else{
                    b = s[p] / 2;
                    link[i[b]] = 1 - link[i[b]];
                    link[j[b]] = 1 - link[j[b]];
                    link[k[b]] = 1 - link[k[b]];
                    link[l[b]] = 1 - link[l[b]];
                    
                }
                s_new[p] = s[p];
            }
            if (n < 0){
                cout << "n " << x << " < 0" << endl;
            }
            /*cout << "operator configuration after diagonal update" << endl;
             for (p = 0; p < M; p ++){
             //cout << s[p] << " ";
             }
             //cout << endl;*/
            
            // off-diagonal update
            
            //cout << "off-diagonal update" << endl;
            for (u = 0; u < Nb; u ++){
                if (link[u] != link0[u]){
                    cout << "1error" << endl;
                    MC_sweep = MC_step;
                    break;
                }
            }
            // count the number of flippable plaquette
            for (p = 0; p < M; p ++){
                if (s[p] != -1){
                    b = s[p] / 2;
                    if (s[p] % 2 == 1){
                        link[i[b]] = 1 - link[i[b]];
                        link[j[b]] = 1 - link[j[b]];
                        link[k[b]] = 1 - link[k[b]];
                        link[l[b]] = 1 - link[l[b]];
                    }
                    if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                        num_FP ++;
                    }
                }
            }
            
            //cout << "num of FP = " << num_FP << endl;
            while (a == 0){
                
                // find the starting stack
                
                prob = floor(num_FP * conti(generate));
                //cout << "prob = "  << prob << endl;
                for (u = 0; u < Nb; u ++){
                    updating_line[u] = 0;
                }
                q = 0;
                p = 0;
                //cout << "p = 0" << endl;
                while (q <= prob) {
                    if (s[p] != -1){
                        b = s[p] / 2;
                        if (s[p] % 2 == 1){
                            //cout << "s[p] = " << s[p] << endl;
                            link[i[b]] = 1 - link[i[b]];
                            link[j[b]] = 1 - link[j[b]];
                            link[k[b]] = 1 - link[k[b]];
                            link[l[b]] = 1 - link[l[b]];
                        }
                        if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                            //cout << "q = " << q << endl;
                            q += 1; // we pick the prob-th flippable plaquette...
                        }
                    }
                    p += 1; // which is located in the p-th time stack
                    //cout << "p = " << p << endl;
                    if (p > M){
                        cout << "3error" << endl;
                        MC_sweep = MC_step;
                        break;
                    }
                    
                    if (p > M - 1){
                        continue;
                    }
                }
                
                p -= 1;
                q = p; // record where we start
                //cout << "q = " << q << endl;
                
                
                if (s[q] % 2 == 1){
                    b = s[q] / 2;
                    link[i[b]] = 1 - link[i[b]];
                    link[j[b]] = 1 - link[j[b]];
                    link[k[b]] = 1 - link[k[b]];
                    link[l[b]] = 1 - link[l[b]];
                }
                // copy the configuration to the new array
                for (u = 0; u < Nb; u ++){
                    link_new[u] = link[u];
                }
                
                // start the update
                b = s[p] / 2;
                //cout << "b = " << b << endl;
                // first the old ones...
                if (s[p] % 2 == 1){
                    link[i[b]] = 1 - link[i[b]];
                    link[j[b]] = 1 - link[j[b]];
                    link[k[b]] = 1 - link[k[b]];
                    link[l[b]] = 1 - link[l[b]];
                    
                }
                for (u = 0; u < Nb / 2; u ++){
                    if (link[i[u]] + link[j[u]] + link[k[u]] + link[l[u]] > 2){
                        cout << "error!" << endl;
                        MC_sweep = MC_step;
                        break;
                    }
                }
                
                // and then the new ones...
                updating_line[i[b]] = 1;
                updating_line[j[b]] = 1;
                updating_line[k[b]] = 1;
                updating_line[l[b]] = 1;
                if (s_new[p] % 2 == 0){
                    s_new[p] += 1; // flip the type of the operator
                    link_new[i[b]] = 1 - link_new[i[b]];
                    link_new[j[b]] = 1 - link_new[j[b]];
                    link_new[k[b]] = 1 - link_new[k[b]];
                    link_new[l[b]] = 1 - link_new[l[b]];
                }
                else{
                    s_new[p] -= 1;
                }
                for (u = 0; u < Nb / 2; u ++){
                    if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                        cout << "error!!" << endl;
                        MC_sweep = MC_step;
                        break;
                    }
                }
                
                //cout << "s_new[p] = "<< s_new[p] << endl;
                p = (p + 1) % M;
                
                while (true){
                    //cout << "p = " << p << endl;
                    // AN ADDITIONAL REQUIREMENT: if the updating lines complete the whole imaginary time period
                    if (p == q){
                        //cout << "too long" << endl;
                        for (int i = 0; i < M; i ++){
                            s_new[i] = s[i];
                        }
                        for (int i = 0; i < Nb; i ++){
                            link0new[i] = link0[i];
                            link[i] = link0[i];
                        }
                        num_updating_line = 0; // annihilate all the updating lines
                        break; // we still don't have a complete cluster and thus need to initiate another one (the former one is abandoned)
                    }
                    //cout << "s[p] = " << s[p] << endl;
                    // first the old ones
                    if (s[p] % 2 == 1){
                        int b = s[p] / 2;
                        link[i[b]] = 1 - link[i[b]];
                        link[j[b]] = 1 - link[j[b]];
                        link[k[b]] = 1 - link[k[b]];
                        link[l[b]] = 1 - link[l[b]];
                        
                    }
                    for (int u = 0; u < Nb / 2; u ++){
                        if (link[i[u]] + link[j[u]] + link[k[u]] + link[l[u]] > 2){
                            cout << "error!!!" << endl;
                            MC_sweep = MC_step;
                            break;
                        }
                    }
                    // then the new ones
                    if (p == 0){
                        for (int i = 0; i < Nb; i ++){
                            link0new[i] = link_new[i];
                        }
                    }
                    
                    //cout << endl;
                    if (s_new[p] == -1){
                        //cout << "continue" << endl;
                        p = (p + 1) % M;
                        continue;
                    }
                    b = s_new[p] / 2;
                    //cout << "b = " << b << endl;
                    if (updating_line[i[b]] + updating_line[j[b]] + updating_line[k[b]] + updating_line[l[b]] == 0){
                        if (s_new[p] % 2 == 1){
                            link_new[i[b]] = 1 - link_new[i[b]];
                            link_new[j[b]] = 1 - link_new[j[b]];
                            link_new[k[b]] = 1 - link_new[k[b]];
                            link_new[l[b]] = 1 - link_new[l[b]];
                        }
                        for (int u = 0; u < Nb / 2; u ++){
                            if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                                cout << "error!!!!" << endl;
                                MC_sweep = MC_step;
                                break;
                            }
                        }
                        p = (p + 1)% M;
                        //cout << "continue" << endl;
                        continue;
                    }
                    
                    // case 1
                    if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] != 2 && link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] == 2){ // if it's acting on an NFP before the update
                        //cout << "case 1" << endl;
                        if (conti(generate) < 0.5){
                            if (s_new[p] % 2 == 1){
                                s_new[p] -= 1;
                            }
                            else{
                                s_new[p] += 1;
                                b = s[p] / 2;
                                link_new[i[b]] = 1 - link_new[i[b]];
                                link_new[j[b]] = 1 - link_new[j[b]];
                                link_new[k[b]] = 1 - link_new[k[b]];
                                link_new[l[b]] = 1 - link_new[l[b]];
                            }
                            for (u = 0; u < Nb / 2; u ++){
                                if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                                    cout << "error!!!!!" << endl;
                                    MC_sweep = MC_step;
                                    break;
                                }
                            }
                        }
                    }
                    
                    // case 2
                    if (link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] != 2){
                        //cout << "case 2" << endl;
                        if (s_new[p] % 2 == 1){
                            s_new[p] -= 1;
                        }
                    }
                    
                    // case 3
                    if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2 && link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] == 2){
                        //cout << "case 3" << endl;
                        a = 0;
                        for (u = 0; u < Nb; u ++){
                            if (updating_line[u] == 1){
                                a ++;
                            }
                        }
                        if (a == 4){
                            updating_line[i[b]] = 0;
                            updating_line[j[b]] = 0;
                            updating_line[k[b]] = 0;
                            updating_line[l[b]] = 0;
                            if (s_new[p] % 2 == 0){
                                s_new[p] += 1;
                                //cout << "s_new[p] = " << s_new[p] << endl;
                                link_new[i[b]] = 1 - link_new[i[b]];
                                link_new[j[b]] = 1 - link_new[j[b]];
                                link_new[k[b]] = 1 - link_new[k[b]];
                                link_new[l[b]] = 1 - link_new[l[b]];
                                for (u = 0; u < Nb / 2; u ++){
                                    if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                                        cout << "error!!!!!!" << endl;
                                        MC_sweep = MC_step;
                                        break;
                                    }
                                }
                            }
                            else{
                                s_new[p] -= 1;
                                //cout << "s_new[p] = " << s_new[p] << endl;
                            }
                            a = 1; // now we have a complete close cluster
                            //cout << "end" << endl;
                            break;
                        }
                        else{
                            a = 0;
                            if (s_new[p] % 2 == 1){
                                link_new[i[b]] = 1 - link_new[i[b]];
                                link_new[j[b]] = 1 - link_new[j[b]];
                                link_new[k[b]] = 1 - link_new[k[b]];
                                link_new[l[b]] = 1 - link_new[l[b]];
                            }
                        }
                    }
                    if (link_new[i[b]] != link[i[b]]){
                        updating_line[i[b]] = 1;
                    }
                    else{
                        updating_line[i[b]] = 0;
                    }
                    if (link_new[j[b]] != link[j[b]]){
                        updating_line[j[b]] = 1;
                    }
                    else{
                        updating_line[j[b]] = 0;
                    }
                    if (link_new[k[b]] != link[k[b]]){
                        updating_line[k[b]] = 1;
                    }
                    else{
                        updating_line[k[b]] = 0;
                    }
                    if (link_new[l[b]] != link[l[b]]){
                        updating_line[l[b]] = 1;
                    }
                    else{
                        updating_line[l[b]] = 0;
                    }
                    p = (p + 1) % M;
                }
            }
            a = 0;
            
            // determine whether the update is accepted (in this part because we have to keep the time stack p where the update stops, all the dummy variables are replaced by u
            
            ////count the new number of FP's
            //first we should move back to the 0-th stack
            for (u = 0; u < Nb; u ++){
                link_new[u] = link0new[u];
            }
            for (p = 0; p < M; p ++){
                if (s_new[p] != -1){
                    b = s_new[p] / 2;
                    if (s_new[p] % 2 == 1){
                        link_new[i[b]] = 1 - link_new[i[b]];
                        link_new[j[b]] = 1 - link_new[j[b]];
                        link_new[k[b]] = 1 - link_new[k[b]];
                        link_new[l[b]] = 1 - link_new[l[b]];
                    }
                    if (link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] == 2){
                        num_FP_after ++;
                    }
                }
            }
            prob = conti(generate);
            /*cout << "FP = " << num_FP << endl;
             //cout << "new FP = " << num_FP_after << endl;
             //cout << "delta = " << num_FP_after - num_FP << endl;
             //cout << "prob = " << (float)num_FP / num_FP_after * pow((2 / (1 + V)), (num_FP_after - num_FP)) << endl;*/
            if (prob < (float)num_FP / num_FP_after * pow((2 / (1 + V)), (num_FP_after - num_FP))){ // accepted!!!
                //cout << "accept" << endl;
                for (u = 0; u < M; u ++){
                    s[u] = s_new[u];
                }
                for (u = 0; u < Nb; u ++){
                    link0[u] = link0new[u];
                }
                /*for (u = 0; u < M; u ++){
                 //cout << s[u] << " ";
                 }
                 //cout << endl;*/
                
            }
            else{
                //cout << "reject " << endl;
                for (u = 0; u < M; u ++){
                    s_new[u] = s[u];
                }
                //cout << "link0new" << endl;
                for (u = 0; u < Nb; u ++){
                    link0new[u] = link0[u];
                    
                }
            }
            // move back to the 0-th stack
            for (u = 0; u < Nb; u++){
                link[u] = link0[u];
            }
            /*cout << "the updated operator configuration" << endl;
             for (u = 0; u < M; u ++){
             //cout << s[u] << " ";
             }*/
            /*for (int p = 0; p < M; p ++){
             
             if (s[p] % 2 == 1){
             //cout << "p = " << p << endl;
             b = s[p] / 2;
             link[i[b]] = 1 - link[i[b]];
             link[j[b]] = 1 - link[j[b]];
             link[k[b]] = 1 - link[k[b]];
             link[l[b]] = 1 - link[l[b]];
             
             }
             }
             for (u = 0; u < Nb; u ++){
             if (link[u] != link0[u]){
             //cout << "error!!!!!!!" << endl;
             }
             }*/
            
            num_FP = 0;
            num_FP_after = 0;
            
            
            
            if ((float)n / M > 0.75){ // expansion
                M = ceil(4.0 / 3.0 * M);
                //cout << "new M " << x << " = " << M << endl;
            }
            
            // move back the configuration and operator string
            
            /*for (int i = 0; i < Nb; i ++){
             linklink[x][i] = link[i];
             }*/
            /*for (int i = 0; i < M_max; i ++){
             ss[x][i] = s[i];
             }*/
            
            
            
            
            //////
            
            //nn[x] = n;
            //MM[x] = M;
            //////
            
            
            
            
            
            
            //}
            
            // send the parameters from each replicas to master only after the 9th step
            if (step % 10 == 8){
                // measurement
                // measure energy
                E = n * -1.0 / beta / Nb;
                
                // measure VBS parameter
                
                for (p = 0; p < M; p ++){
                    //cout << "p = " << p << endl;
                    if (s[p] != -1){
                        //cout << "OD!!!" << endl;
                        if (s[p] % 2 == 1){
                            b = s[p] / 2;
                            link[i[b]] = 1 - link[i[b]];
                            link[j[b]] = 1 - link[j[b]];
                            link[k[b]] = 1 - link[k[b]];
                            link[l[b]] = 1 - link[l[b]];
                        }
                        for (u = 0; u < N * N; u ++){
                            //cout << "u = " << u << endl;
                            psi_x += phase[u] * (link[u] - link[u / N * N + (u + N - 1) % N]);
                        }
                        for (u = N * N; u < Nb; u ++){
                            //cout << "i = " << i <<  endl;
                            if (u < N * N + N){
                                psi_y += phase[u] * (link[u] - link[u + N * N - N]);
                            }
                            else{
                                psi_y += phase[u] * (link[u] - link[u - N]);
                            }
                        }
                    }
                }
                
                
                avg_x = psi_x / (1.0 * N * N * n);
                avg_y = psi_y / (1.0 * N * N * n);
                psi_x = 0;
                psi_y = 0;
                
                //cout << "wait to send" << endl;
                MPI_Barrier(MPI_COMM_WORLD);
                //cout << "check wagawaga" << endl;
                //cout << "x = " << x << endl;
                //cout << "E = " << E << endl;
                //cout << "n = " << n << endl;
                //cout << "M = " << M << endl;
                
                if (x != master){
                    MPI_Send(&E, 1, MPI_DOUBLE, master, tg_energy, MPI_COMM_WORLD);
                    MPI_Send(&n, 1, MPI_INT, master, tg_n, MPI_COMM_WORLD);
                    MPI_Send(&avg_x, 1, MPI_DOUBLE, master, tg_psix, MPI_COMM_WORLD);
                    MPI_Send(&avg_y, 1, MPI_DOUBLE, master, tg_psiy, MPI_COMM_WORLD);
                    //cout << "sent!" << endl;
                }
                else{
                    //MM[0] = M;
                    nn[0] = n;
                    EE[0] = E;
                    psixpsix[0] = avg_x;
                    psiypsiy[0] = avg_y;
                }
                //cout << "wait to receive" << endl;
                MPI_Barrier(MPI_COMM_WORLD);
                if (x == master){
                    for (u = 1; u < y; u ++){
                        MPI_Recv(&EE[u], 1, MPI_DOUBLE, u, tg_energy, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                        MPI_Recv(&nn[u], 1, MPI_INT, u, tg_n, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                        MPI_Recv(&psixpsix[u], 1, MPI_DOUBLE, u, tg_psix, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                        MPI_Recv(&psiypsiy[u], 1, MPI_DOUBLE, u, tg_psiy, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                        //cout << "received!" << endl;
                    }
                }
                MPI_Barrier(MPI_COMM_WORLD);
                if (x == 0){
                    //cout << "printing time!" << endl;
                    for (u = 0; u < y; u ++){
                        for (v = 0; v < y; v ++){
                            if (betabeta[v] == base + u){
                                myfile << EE[v] << " " << psixpsix[v] << " " << psiypsiy[v] << " ";
                            }
                        }
                        //cout << i << endl;
                        //cout << "n = " << nn[i] << endl;
                        //cout << "M = " << MM[i] << endl;
                    }
                    myfile << endl;
                }
                MPI_Barrier(MPI_COMM_WORLD);
            }
            //myfile << endl;
            //cout << endl;
            //MPI_Barrier(MPI_COMM_WORLD);
            //}
            //MPI_Barrier(MPI_COMM_WORLD);
            
        }
        MPI_Barrier(MPI_COMM_WORLD);
        //cout << "parallel ended" << endl;
    }
    MPI_Finalize();
    
    //}
    
}
