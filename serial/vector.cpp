//
//  main.cpp
//  PT_YJ
//
//  Created by liyuezhen on 2020/4/30.
//  Copyright © 2020 liyuezhen. All rights reserved.
//

#include <iostream>
#include <math.h>
#include <random>
#include <fstream>
#include <string>
#include <vector>
using std::cout;
using std::cin;
using std::endl;
using namespace std;
int main(void){
    fstream myfile;
    if (!myfile){
        //cout << "sth wrong" << endl;
    }
    double prob;
    int M_max = 10000;
    
    std::random_device rd;
    std::mt19937 generate = std::mt19937(rd()); // remember to switch back to rd()
    std::uniform_real_distribution<float> conti(0,1);
    
    // Defining which link belongs to each plaquettes. In the content below, ijkl are ordered by their values from small to large.
    
    
    // Variables declaration
    double beta, V = 0.5;
    int num_FP = 0, num_updating_line = 0, num_FP_after = 0;
    int b, N = 16, y = 100, base = 50; // N: system size, y: number of replicas, n: number of operators
    int MM[y], MM_temp[y], nn[y], number[y];
    int Nb = 2 * N * N;
    int i[N * N], j[N * N], k[N * N], l[N * N]; // the bond surrounding each plaquette.
    int s[M_max], s_new[M_max];
    //int *ss[y], *s_temp[y], *linklink[y], *link_temp[y];
    
    vector<int> row_s;
    vector<int> row_link;
    row_s.assign(M_max, -1);
    row_link.assign(Nb, 0);
    vector<vector<int>> ss;
    vector<vector<int>> s_temp;
    vector<vector<int>> linklink;
    vector<vector<int>> link_temp;
    ss.assign(y, row_s);
    s_temp.assign(y, row_s);
    linklink.assign(y, row_link);
    link_temp.assign(y, row_link);
    
    /*for (int i = 0; i < y; i ++){
        s_temp[i] = (int*)malloc(M_max*sizeof(int*));
        ss[i] = (int*)malloc(M_max*sizeof(int*));
        linklink[i] = (int*)malloc(Nb*sizeof(int*));
        link_temp[i] = (int*)malloc(Nb*sizeof(int*));
    }*/
    
    
    
    int link[Nb], link_new[Nb], link0[Nb], link0new[Nb], updating_line[Nb], phase[Nb]; // updating_line[Nb]: a list recording if there's an updating line at a given link (1 = yes, 0 = no)
    int MC_sweep, MC_step = 3000000;
    int a = 0; // to determine if we have an acceptable off-diagonal update
    int bin;
    int psi_x = 0, psi_y = 0;
    float avg_x = 0, avg_y = 0;
    
    for (int p = 0; p < N * N; p ++){
        i[p] = p;
        j[p] = (p + N) % (N * N);
        k[p] = p + N * N;
        l[p] = k[p] / N * N + (k[p] % N + 1) % N;
        //cout << i[p] << " " << j[p] << " " << k[p] << " "  << l[p] << endl;
    }
    
    for (int x = 0; x < y; x ++){ // x: the number of replica
        number[x] = x;
        MM[x] = 3000;
        MM_temp[x] = 3000;
        nn[x] = 0;
        for (int p = 0; p < M_max; p ++){
            ss[x][p] = -1;
        }
        if (x % 4 == 0){ // start from different configurations
            for (int p = 0; p < Nb; p ++){
                if (p < Nb / 2 && p % 2 == 0){
                    linklink[x][p] = 1;
                }
                else{
                    linklink[x][p] = 0;
                }
            }
        }
        if (x % 4 == 1){
            for (int p = 0; p < Nb; p ++){
                if (p < Nb / 2 && p % 2 == 1){
                    linklink[x][p] = 1;
                }
                else{
                    linklink[x][p] = 0;
                }
            }
        }
        if (x % 4 == 2){
            for (int p = 0; p < Nb; p ++){
                if (p >= Nb / 2 && (p / N) % 2 == 0){
                    linklink[x][p] = 1;
                }
                else{
                    linklink[x][p] = 0;
                }
            }
        }
        if (x % 4 == 3){
            for (int p = 0; p < Nb; p ++){
                if (p >= Nb / 2 && (p / N) % 2 == 1){
                    linklink[x][p] = 1;
                }
                else{
                    linklink[x][p] = 0;
                }
            }
        }
    }
    
    for (int i = 0; i < Nb; i ++){
        if (i < Nb / 2){
            phase[i] = pow(-1, i);
        }
        else{
            phase[i] = pow(-1, i / N);
        }
    }
    
    
    
    // rotate!
    /*
    p = N * N;
    q = N * N + N - 1;
    for (x = 0; x < y; x ++){
        linklink[x][p] = 1;
        linklink[x][q] = 1;
        for (u = p - N * N; u <= q - N * N - 1; u ++){
            linklink[x][u] = 1 - linklink[x][u];
        }
        if (p / N == 2 * N - 1){ // to deal with the rightmost column...
            for (u = p - N * N - N * (N - 1); u <= q - N * N - N * (N - 1) - 1; u ++){
                linklink[x][u] = 1 - linklink[x][u];
            }
        }
        else{
            for (u = p - N * N + N; u <= q - N * N + N - 1; u ++){
                linklink[x][u] = 1 - linklink[x][u];
            }
        }
    }
    */
    
    
    
    
    
    // start the update
    //for (bin = 0; bin < 1; bin ++){
    //cout << "bin = " << bin << endl;
    
    for (MC_sweep = 0; MC_sweep < MC_step; MC_sweep ++){
        //cout << "MC sweep = " << MC_sweep << endl;
        if (MC_sweep % 10 == 0){ // conducting the swap step every 10 MC sweep
            for (int i = 0; i < y - 1; i ++){
                prob = conti(generate);
                //cout << nn[i] << " " << nn[i + 1] << endl;
                //cout << "prob = " << pow(1.0 * (base + i + 1) / (base + i), nn[i] - nn[i + 1]) << endl;
                if (prob < pow(1.0 * (base + i + 1) / (base + i), nn[i] - nn[i + 1])){
                    int l = number[i];
                    number[i] = number[i + 1];
                    number[i + 1] = l;
                    l = nn[i];
                    nn[i] = nn[i + 1];
                    nn[i + 1] = l;
                    //cout << "swap " << i << " " << i + 1 << endl;
                }
            }
            
            // rearrange the order of operator string and configurations according to the order
            
            for (int i = 0; i < y; i ++){
                MM_temp[i] = MM[number[i]];
                for (int j = 0; j < Nb; j ++){
                    link_temp[i][j] = linklink[number[i]][j];
                }
                for (int j = 0; j < M_max; j ++){
                    s_temp[i][j] = ss[number[i]][j];
                }
            }
            
            for (int i = 0; i < y; i ++){
                cout << number[i] << " ";
                MM[i] = MM_temp[i];
                for (int j = 0; j < Nb; j ++){
                    linklink[i][j] = link_temp[i][j];
                }
                for (int j = 0; j < M_max; j ++){
                    ss[i][j] = s_temp[i][j];
                }
                number[i] = i;
            }
            cout << endl;
        }
        else{ // parallel
            
            for (int x = 0; x < y; x ++){ // x: number of replica
                //cout << "x = " << x << endl;
                
                beta = (base + x) / 10.0;
                int n = nn[x];
                int M = MM[x];
                
                // move all the variables to the public one
                //cout << "previous configuration" << endl;
                for (int i = 0; i < Nb; i ++){
                    link[i] = linklink[x][i];
                    link0[i] = link[i];
                    link0new[i] = link[i];
                    //cout << link[i] << " ";
                }
                //cout << endl;
                
                //cout << "previous operator string" << endl;
                for (int p = 0; p < M_max; p ++){
                    s[p] = ss[x][p];
                    //cout << s[p] << " ";
                }
                //cout << endl;
                
                // here we start a normal MC step
                /*q = 0;
                 for (int i = 0; i < Nb; i ++){
                 if (link[i] == 1){
                 q ++;
                 }
                 }
                 if (q != Nb / 4){
                 for (u = 0; u < 100; u ++){
                 cout << "error" << endl;
                 }
                 break;
                 }*/
                //cout << "n = " << n << endl;
                // diagonal update
                /*cout << "diagonal update" << endl;
                 //cout << "initial state" << endl;
                 for (u = 0; u < Nb; u ++){
                 if (link[u] == 1){
                 //cout << u << " ";
                 }
                 }
                 //cout << endl;*/
                for (int p = 0; p < M; p ++){
                    //cout << "p = " << p << endl;
                    if (s[p] == -1){
                        //cout << "no operator" << endl;
                        b = floor(N * N * conti(generate)); // choose a plaquette
                        //cout << "b = " << b << endl;
                        if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                            //cout << "flippable" << endl;
                            if (conti(generate) < N * N * beta / (M - n)){
                                //cout << "insert an FP" << endl;
                                s[p] = 2 * b;
                                //cout << "s" << p << " = " << s[p] << endl;
                                n += 1;
                            }
                            //cout << "n = " << n << endl;
                        }
                        else{
                            if (conti(generate) < N * N * beta * (1 + V) / (M - n)){
                                s[p] = 2 * b;
                                //cout << "s" << p << " = " << s[p] << endl;
                                n += 1;
                                //cout << "n = " << n << endl;
                            }
                        }
                    }
                    else if (s[p] % 2 == 0){
                        b = s[p] / 2;
                        if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                            if (conti(generate) < (M - n + 1) / (N * N * beta)){
                                s[p] = -1;
                                //cout << "s" << p << " = " << s[p] << endl;
                                n -= 1;
                            }
                            //cout << "n = " << n << endl;
                        }
                        else{
                            if (conti(generate) < (M - n + 1) / (N * N * beta * (1 + V))){
                                s[p] = -1;
                                //cout << "s" << p << " = " << s[p] << endl;
                                n -= 1;
                                //cout << "n = " << n << endl;
                            }
                        }
                    }
                    else{
                        b = s[p] / 2;
                        link[i[b]] = 1 - link[i[b]];
                        link[j[b]] = 1 - link[j[b]];
                        link[k[b]] = 1 - link[k[b]];
                        link[l[b]] = 1 - link[l[b]];
                        
                    }
                    s_new[p] = s[p];
                }
                
                /*cout << "operator configuration after diagonal update" << endl;
                 for (p = 0; p < M; p ++){
                 //cout << s[p] << " ";
                 }
                 //cout << endl;*/
                
                // off-diagonal update
                
                //cout << "off-diagonal update" << endl;
                for (int i = 0; i < Nb; i ++){
                    if (link[i] != link0[i]){
                        cout << "1error" << endl;
                        
                    }
                }
                // count the number of flippable plaquette
                for (int p = 0; p < M; p ++){
                    if (s[p] != -1){
                        b = s[p] / 2;
                        if (s[p] % 2 == 1){
                            link[i[b]] = 1 - link[i[b]];
                            link[j[b]] = 1 - link[j[b]];
                            link[k[b]] = 1 - link[k[b]];
                            link[l[b]] = 1 - link[l[b]];
                        }
                        if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                            num_FP ++;
                        }
                    }
                }
                
                //cout << "num of FP = " << num_FP << endl;
                while (a == 0){
                    
                    // find the starting stack
                    
                    prob = floor(num_FP * conti(generate));
                    //cout << "prob = "  << prob << endl;
                    for (int i = 0; i < Nb; i ++){
                        updating_line[i] = 0;
                    }
                    int q = 0;
                    int p = 0;
                    //cout << "p = 0" << endl;
                    while (q <= prob) {
                        if (s[p] != -1){
                            b = s[p] / 2;
                            if (s[p] % 2 == 1){
                                //cout << "s[p] = " << s[p] << endl;
                                link[i[b]] = 1 - link[i[b]];
                                link[j[b]] = 1 - link[j[b]];
                                link[k[b]] = 1 - link[k[b]];
                                link[l[b]] = 1 - link[l[b]];
                            }
                            if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2){
                                //cout << "q = " << q << endl;
                                q += 1; // we pick the prob-th flippable plaquette...
                            }
                        }
                        p += 1; // which is located in the p-th time stack
                        //cout << "p = " << p << endl;
                        if (p > M){
                            cout << "3error" << endl;
                        }
                        
                        if (p > M - 1){
                            continue;
                        }
                    }
                    
                    p -= 1;
                    q = p; // record where we start
                    //cout << "q = " << q << endl;
                    
                    
                    if (s[q] % 2 == 1){
                        b = s[q] / 2;
                        link[i[b]] = 1 - link[i[b]];
                        link[j[b]] = 1 - link[j[b]];
                        link[k[b]] = 1 - link[k[b]];
                        link[l[b]] = 1 - link[l[b]];
                    }
                    // copy the configuration to the new array
                    for (int i = 0; i < Nb; i ++){
                        link_new[i] = link[i];
                    }
                    
                    // start the update
                    b = s[p] / 2;
                    //cout << "b = " << b << endl;
                    // first the old ones...
                    if (s[p] % 2 == 1){
                        link[i[b]] = 1 - link[i[b]];
                        link[j[b]] = 1 - link[j[b]];
                        link[k[b]] = 1 - link[k[b]];
                        link[l[b]] = 1 - link[l[b]];
                        
                    }
                    for (int u = 0; u < Nb / 2; u ++){
                        if (link[i[u]] + link[j[u]] + link[k[u]] + link[l[u]] > 2){
                            cout << "error!" << endl;
                        }
                    }
                    
                    // and then the new ones...
                    updating_line[i[b]] = 1;
                    updating_line[j[b]] = 1;
                    updating_line[k[b]] = 1;
                    updating_line[l[b]] = 1;
                    if (s_new[p] % 2 == 0){
                        s_new[p] += 1; // flip the type of the operator
                        link_new[i[b]] = 1 - link_new[i[b]];
                        link_new[j[b]] = 1 - link_new[j[b]];
                        link_new[k[b]] = 1 - link_new[k[b]];
                        link_new[l[b]] = 1 - link_new[l[b]];
                    }
                    else{
                        s_new[p] -= 1;
                    }
                    for (int u = 0; u < Nb / 2; u ++){
                        if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                            cout << "error!!" << endl;
                        }
                    }
                    
                    //cout << "s_new[p] = "<< s_new[p] << endl;
                    p = (p + 1) % M;
                    
                    while (true){
                        //cout << "p = " << p << endl;
                        // AN ADDITIONAL REQUIREMENT: if the updating lines complete the whole imaginary time period
                        if (p == q){
                            //cout << "too long" << endl;
                            for (int i = 0; i < M; i ++){
                                s_new[i] = s[i];
                            }
                            for (int i = 0; i < Nb; i ++){
                                link0new[i] = link0[i];
                                link[i] = link0[i];
                            }
                            num_updating_line = 0; // annihilate all the updating lines
                            break; // we still don't have a complete cluster and thus need to initiate another one (the former one is abandoned)
                        }
                        //cout << "s[p] = " << s[p] << endl;
                        // first the old ones
                        if (s[p] % 2 == 1){
                            b = s[p] / 2;
                            link[i[b]] = 1 - link[i[b]];
                            link[j[b]] = 1 - link[j[b]];
                            link[k[b]] = 1 - link[k[b]];
                            link[l[b]] = 1 - link[l[b]];
                            
                        }
                        for (int u = 0; u < Nb / 2; u ++){
                            if (link[i[u]] + link[j[u]] + link[k[u]] + link[l[u]] > 2){
                                cout << "error!!!" << endl;
                            }
                        }
                        // then the new ones
                        if (p == 0){
                            for (int i = 0; i < Nb; i ++){
                                link0new[i] = link_new[i];
                            }
                        }
                        
                        //cout << endl;
                        if (s_new[p] == -1){
                            //cout << "continue" << endl;
                            p = (p + 1) % M;
                            continue;
                        }
                        b = s_new[p] / 2;
                        //cout << "b = " << b << endl;
                        if (updating_line[i[b]] + updating_line[j[b]] + updating_line[k[b]] + updating_line[l[b]] == 0){
                            if (s_new[p] % 2 == 1){
                                link_new[i[b]] = 1 - link_new[i[b]];
                                link_new[j[b]] = 1 - link_new[j[b]];
                                link_new[k[b]] = 1 - link_new[k[b]];
                                link_new[l[b]] = 1 - link_new[l[b]];
                            }
                            for (int u = 0; u < Nb / 2; u ++){
                                if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                                    cout << "error!!!!" << endl;
                                }
                            }
                            p = (p + 1)% M;
                            //cout << "continue" << endl;
                            continue;
                        }
                        
                        // case 1
                        if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] != 2 && link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] == 2){ // if it's acting on an NFP before the update
                            //cout << "case 1" << endl;
                            if (conti(generate) < 0.5){
                                if (s_new[p] % 2 == 1){
                                    s_new[p] -= 1;
                                }
                                else{
                                    s_new[p] += 1;
                                    b = s[p] / 2;
                                    link_new[i[b]] = 1 - link_new[i[b]];
                                    link_new[j[b]] = 1 - link_new[j[b]];
                                    link_new[k[b]] = 1 - link_new[k[b]];
                                    link_new[l[b]] = 1 - link_new[l[b]];
                                }
                                for (int u = 0; u < Nb / 2; u ++){
                                    if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                                        cout << "error!!!!!" << endl;
                                    }
                                }
                            }
                        }
                        
                        // case 2
                        if (link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] != 2){
                            //cout << "case 2" << endl;
                            if (s_new[p] % 2 == 1){
                                s_new[p] -= 1;
                            }
                        }
                        
                        // case 3
                        if (link[i[b]] + link[j[b]] + link[k[b]] + link[l[b]] == 2 && link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] == 2){
                            //cout << "case 3" << endl;
                            a = 0;
                            for (int i = 0; i < Nb; i ++){
                                if (updating_line[i] == 1){
                                    a ++;
                                }
                            }
                            if (a == 4){
                                updating_line[i[b]] = 0;
                                updating_line[j[b]] = 0;
                                updating_line[k[b]] = 0;
                                updating_line[l[b]] = 0;
                                if (s_new[p] % 2 == 0){
                                    s_new[p] += 1;
                                    //cout << "s_new[p] = " << s_new[p] << endl;
                                    link_new[i[b]] = 1 - link_new[i[b]];
                                    link_new[j[b]] = 1 - link_new[j[b]];
                                    link_new[k[b]] = 1 - link_new[k[b]];
                                    link_new[l[b]] = 1 - link_new[l[b]];
                                    for (int u = 0; u < Nb / 2; u ++){
                                        if (link_new[i[u]] + link_new[j[u]] + link_new[k[u]] + link_new[l[u]] > 2){
                                            cout << "error!!!!!!" << endl;
                                        }
                                    }
                                }
                                else{
                                    s_new[p] -= 1;
                                    //cout << "s_new[p] = " << s_new[p] << endl;
                                }
                                a = 1; // now we have a complete close cluster
                                //cout << "end" << endl;
                                break;
                            }
                            else{
                                a = 0;
                                if (s_new[p] % 2 == 1){
                                    link_new[i[b]] = 1 - link_new[i[b]];
                                    link_new[j[b]] = 1 - link_new[j[b]];
                                    link_new[k[b]] = 1 - link_new[k[b]];
                                    link_new[l[b]] = 1 - link_new[l[b]];
                                }
                            }
                        }
                        if (link_new[i[b]] != link[i[b]]){
                            updating_line[i[b]] = 1;
                        }
                        else{
                            updating_line[i[b]] = 0;
                        }
                        if (link_new[j[b]] != link[j[b]]){
                            updating_line[j[b]] = 1;
                        }
                        else{
                            updating_line[j[b]] = 0;
                        }
                        if (link_new[k[b]] != link[k[b]]){
                            updating_line[k[b]] = 1;
                        }
                        else{
                            updating_line[k[b]] = 0;
                        }
                        if (link_new[l[b]] != link[l[b]]){
                            updating_line[l[b]] = 1;
                        }
                        else{
                            updating_line[l[b]] = 0;
                        }
                        p = (p + 1) % M;
                    }
                }
                a = 0;
                
                // determine whether the update is accepted (in this part because we have to keep the time stack p where the update stops, all the dummy variables are replaced by u
                
                ////count the new number of FP's
                //first we should move back to the 0-th stack
                for (int i = 0; i < Nb; i ++){
                    link_new[i] = link0new[i];
                }
                for (int p = 0; p < M; p ++){
                    if (s_new[p] != -1){
                        int b = s_new[p] / 2;
                        if (s_new[p] % 2 == 1){
                            link_new[i[b]] = 1 - link_new[i[b]];
                            link_new[j[b]] = 1 - link_new[j[b]];
                            link_new[k[b]] = 1 - link_new[k[b]];
                            link_new[l[b]] = 1 - link_new[l[b]];
                        }
                        if (link_new[i[b]] + link_new[j[b]] + link_new[k[b]] + link_new[l[b]] == 2){
                            num_FP_after ++;
                        }
                    }
                }
                prob = conti(generate);
                /*cout << "FP = " << num_FP << endl;
                 //cout << "new FP = " << num_FP_after << endl;
                 //cout << "delta = " << num_FP_after - num_FP << endl;
                 //cout << "prob = " << (float)num_FP / num_FP_after * pow((2 / (1 + V)), (num_FP_after - num_FP)) << endl;*/
                if (prob < (float)num_FP / num_FP_after * pow((2 / (1 + V)), (num_FP_after - num_FP))){ // accepted!!!
                    //cout << "accept" << endl;
                    for (int i = 0; i < M; i ++){
                        s[i] = s_new[i];
                    }
                    for (int i = 0; i < Nb; i ++){
                        link0[i] = link0new[i];
                    }
                    /*for (u = 0; u < M; u ++){
                     //cout << s[u] << " ";
                     }
                     //cout << endl;*/
                    
                }
                else{
                    //cout << "reject " << endl;
                    for (int i= 0; i < M; i ++){
                        s_new[i] = s[i];
                    }
                    //cout << "link0new" << endl;
                    for (int i = 0; i < Nb; i ++){
                        link0new[i] = link0[i];
                        
                    }
                }
                // move back to the 0-th stack
                for (int i = 0; i < Nb; i++){
                    link[i] = link0[i];
                }
                /*cout << "the updated operator configuration" << endl;
                 for (u = 0; u < M; u ++){
                 //cout << s[u] << " ";
                 }*/
                /*for (int p = 0; p < M; p ++){
                 
                 if (s[p] % 2 == 1){
                 //cout << "p = " << p << endl;
                 b = s[p] / 2;
                 link[i[b]] = 1 - link[i[b]];
                 link[j[b]] = 1 - link[j[b]];
                 link[k[b]] = 1 - link[k[b]];
                 link[l[b]] = 1 - link[l[b]];
                 
                 }
                 }
                 for (u = 0; u < Nb; u ++){
                 if (link[u] != link0[u]){
                 //cout << "error!!!!!!!" << endl;
                 }
                 }*/
                
                num_FP = 0;
                num_FP_after = 0;
                
                // measurement
                /*for (int p = 0; p < M; p ++){
                    //cout << "p = " << p << endl;
                    if (s[p] != -1 && s[p] % 2 == 1){
                        //cout << "OD!!!" << endl;
                        b = s[p] / 2;
                        link[i[b]] = 1 - link[i[b]];
                        link[j[b]] = 1 - link[j[b]];
                        link[k[b]] = 1 - link[k[b]];
                        link[l[b]] = 1 - link[l[b]];
                        
                    }
                    for (int i = 0; i < N * N; i ++){
                        //cout << "u = " << u << endl;
                        psi_x += phase[i] * (link[i] - link[i / N * N + (i + N - 1) % N]);
                    }
                    for (int i = N * N; i < Nb; i ++){
                        //cout << "i = " << i <<  endl;
                        if (i < N * N + N){
                            psi_y += phase[i] * (link[i] - link[i + N * N - N]);
                        }
                        else{
                            psi_y += phase[i] * (link[i] - link[i - N]);
                        }
                    }
                }
                
                avg_x = psi_x / (1.0 * N * N * M);
                avg_y = psi_y / (1.0 * N * N * M);
                myfile << avg_x << " " << avg_y << " ";
                //cout << avg_x << " " << avg_y << " ";
                psi_x = 0;
                psi_y = 0;*/
                
                
                if ((float)n / M > 0.75){ // expansion
                    M = ceil(4.0 / 3.0 * M);
                    //cout << "new M " << beta << " = " << M << endl;
                }
                
                // move back the configuration and operator string
                
                for (int i = 0; i < Nb; i ++){
                    linklink[x][i] = link[i];
                }
                for (int i = 0; i < M_max; i ++){
                    ss[x][i] = s[i];
                }
                nn[x] = n;
                MM[x] = M;
            }
            
            
            myfile << endl;
            //cout << endl;
        }
    }
    
    //}
}



