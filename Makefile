#CXX=g++ 
DEBUG=-g
#SRC=serial
CXXFLAGS=-std=c++11 -O0
CXX=mpiCC
SRC=MPI

$(SRC)/%.o: $(SRC)/%.cpp
	$(CXX) $(CXXFLAGS) -c $(DEBUG) -o $@ $^ 

pt.e: $(SRC)/vector.o
	$(CXX) $(CXXFLAGS)  $(DEBUG) -o $@ $^

ptp.e: $(SRC)/main.o
	$(CXX) $(CXXFLAGS)  $(DEBUG) -o $@ $^

.phony: clean

clean: 
	rm *.o *.e
